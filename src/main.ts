import { WatcherCreator } from './base/creator';
import ConfigEnv from './base/envMethods';

async function run() {
  const envConfig = new ConfigEnv()
  
  const watcherName = await envConfig.getWatcherName()

	const watcher = WatcherCreator.create(watcherName);
  
  watcher.runner()
}


run();
