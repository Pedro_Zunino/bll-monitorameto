import { BLL } from "../watchers/bll.watcher";

export class WatcherCreator {
    
	private static crawlers = {
		bll : BLL
	};

	public static create(
		crawler: string,
	) {
		return new this.crawlers[crawler.toLowerCase()];
	}
}
