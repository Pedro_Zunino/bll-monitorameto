import { config } from 'dotenv';

const WATCHER='WATCHER'

export default class ConfigEnv{
    constructor() {
        config({'path':"./.env"});
	}

    
    async get(key: string) {
        const value = process.env[key];
        
        if (!value) {
            throw new Error(`Environment variable ${key} not found.`);
        }
        
        return value.trim();
    }
    
    async getWatcherName() {
        return await this.get(WATCHER);
    }
}