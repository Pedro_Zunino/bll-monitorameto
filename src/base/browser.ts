import moment from 'moment-timezone';
import { Browser, Frame, launch, Page } from 'puppeteer';
import cheerio, { CheerioAPI } from 'cheerio';

export class watcherBrowser {
	public page: Page;
	public browser: Browser;
	public context: Frame;

	public async newBrowser() {
		console.log('Creating browser');

		this.browser = await launch({
			headless: false,
			ignoreHTTPSErrors: false,
			args: [
				'--no-zygote',
				'--single-process',
				'--no-sandbox',
				'--start-maximized',
				'--disable-setuid-sandbox',
				'--disable-dev-shm-usage',
				'--lang=pt',
			],
		});

		console.log('Browser created');

		this.page = await this.browser.newPage();

		await this.page.setViewport({ width: 1024, height: 768 });

		console.log('Page created');
		return;
	}

	async getCheerio(selector: string) {
		const html = await this.page.evaluate(
			selector => document.querySelector(`${selector}`).outerHTML,
			selector,
		);

		return cheerio.load(html);
	}
}
