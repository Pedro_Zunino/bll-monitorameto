import { Page, Target } from 'puppeteer';
import { watcherBrowser } from '../base/browser';
import cheerio, { CheerioAPI, Element } from 'cheerio';
import { sleep } from '../base/sleep.helper';
import moment from 'moment';


export class BLL extends watcherBrowser {

	async runner() {

		await this.start('https://bllcompras.com/Process/ProcessSearchPublic?param1=1');
		await this.page.screenshot({ path: './test.jpg' });

		// await this.setFilter();

		// await this.waitLoad();

		// const urls = await this.getOpenOpportunitiesURL();
		// console.log(`Found ${urls.length} valid details url`);
	}

	async start(url: string) {
		await this.newBrowser();
		await this.page.goto(url);
		return;
	}

	async setFilter() {
		const openAdvanceFilters = async () => {
			await this.page
				.waitForSelector('table.table.table-form a[title*="BUSCA"]')
				.then(button => button.click());

			await sleep(2000);

			return;
		};

		const setOnNegotiation = async () => {
			await this.page.waitForSelector('#fkStatus').then(selectElement => selectElement.select('7'));

			return;
		};

		const setNegotiationInCurrentDate = async () => {
			const currentDate = moment().format('DD/MM/YYYY')

      await this.page.evaluate(currentDate=>{
        const negotiationDateFilter = document.querySelector('input#DateEndDispute')

        negotiationDateFilter.setAttribute('value',currentDate)
      },currentDate);

			return;
		};

    const confirmFilters = async () =>{
      await this.page
			.waitForSelector('#btnAuctionSearch')
			.then(confirmButton => confirmButton.click());

    }

		await openAdvanceFilters();

		await setOnNegotiation();

    await setNegotiationInCurrentDate();

    await confirmFilters()
		return;
	}

	async waitLoad() {
		let count = 0;

		do {
			count++;
			console.log(`waiting 7 seconds.. (${count})/5`);

			const load = await this.page.$eval('#loadingModal', loadElement =>
				loadElement.getAttribute('display'),
			);

			if (load == null) break;

			await sleep(7000);

			if (count >= 5) throw new Error('Load time out');
		} while (true);

		return await sleep(5000);
	}

  async filterOpportunitiesOnNegociations(){
    const mainDoc: CheerioAPI = await this.getCheerio('#auctionTable');




  }
	async getOpenOpportunitiesURL(): Promise<string[]> {
    const mainDoc: CheerioAPI = await this.getCheerio('#auctionTable');

		const open_opportunitiesPosition: number[] = await this.getOpenOpportunitiesPosition(mainDoc);

		const all_opportunitiesButtons: Element[] = mainDoc('#tableProcessDataBody')
			.find('a')
			.toArray();
		console.log(`Found ${all_opportunitiesButtons.length} details`);

		const open_opportunitiesDetailsEndPoint: string[] = open_opportunitiesPosition.map(position => {
			const href = mainDoc(all_opportunitiesButtons[position]).attr('href');

			return `https://bllcompras.com/Process${href}`;
		});

		return open_opportunitiesDetailsEndPoint;
	}

	async getOpenOpportunitiesPosition(mainDoc: CheerioAPI): Promise<number[]> {
		const allOpportunitiesElements: Element[] = mainDoc('#tableProcessDataBody')
			.find('tr>td:nth-of-type(7)')
			.toArray();

		console.log(`Found ${allOpportunitiesElements.length} opportunities`);

		const currentTime: number = await this.getCurrentDateAtBLL();
		console.log(`Current time: ${currentTime}`);

		const validOpportunities: Element[] = allOpportunitiesElements.filter(opportunityElement => {
			const opportunityTime = mainDoc(opportunityElement)
				.text()
				.split(' ')
				.pop()
				.split(':')
				.shift();

			return parseInt(opportunityTime) * 100 == currentTime;
		});

		const validOpportunitiesPosition: number[] = validOpportunities.map(opportunity =>
			allOpportunitiesElements.indexOf(opportunity),
		);

		return validOpportunitiesPosition;
	}

	async getCurrentDateAtBLL(): Promise<number> {
		return await this.page.$eval('#dateNowSpan', dateElement => {
			const justTime: string = dateElement.textContent.trim().split(' ').pop();
			//14:23:07
			console.log(`Just time: ${justTime}`);

			const hourMinutesSeconds: number = parseInt(justTime.split(':').shift());
			//14
			console.log(`Just hourMinutesSeconds: ${hourMinutesSeconds}`);

			const countableTime = hourMinutesSeconds * 100;
			//1400
			console.log(`Just countableTime: ${countableTime}`);

			return countableTime;
		});
	}
}
